export const state = () => ({
  projects: [],
  project: {}
})

export const mutations = {
  set (state, projects) {
    state.projects = projects
  },
  add (state, value) {
    // merge(state.projects, value)
    state.projects.push(value)
  },
  remove (state, { project }) {
    state.projects.splice(state.list.indexOf(project), 1)
  },
  setCurrentProject (state, payload) {
    state.project = payload
  },
  removeCurrentProject (state) {
    state.project = {}
  }
}

export const actions = {
  async getProjects ({ state, commit }) {
    if (state.projects.length) {
      return
    }
    try {
      const includes = 'include=field_hero_image,field_project_intro_image'
      const fields = 'fields[node--project]=title,body,field_hero_image,field_project_intro_image&fields[file--file]=uri'
      const query = `?sort=-created&${fields}&${includes}`
      const resp = await this.$portfolioRepository.show(query)
      const data = resp.data
      const included = resp.included
      const projects = []
      data.forEach((item) => {
        let heroImage = ''
        let introImage = ''
        if (item.relationships.field_hero_image || item.relationships.field_project_intro_image) {
          included.forEach((inc) => {
            if (inc.id === item.relationships.field_hero_image.data.id) {
              heroImage = inc.attributes.uri.url
            }
            if (inc.id === item.relationships.field_project_intro_image.data.id) {
              introImage = inc.attributes.uri.url
            }
          })
        }
        projects.push({
          id: item.id,
          title: item.attributes.title,
          summary: item.attributes.body.summary,
          body: item.attributes.body.processed,
          hero_image: {
            alt: item.relationships.field_hero_image.data.meta.alt,
            url: heroImage
          },
          intro_image: {
            alt: item.relationships.field_project_intro_image.data.meta.alt,
            url: introImage
          }
        })
      })
      commit('set', projects)
    } catch (err) {
      // eslint-disable-next-line
      console.log(err)
    }
  },
  async getCurrentProject ({ state, commit }, id) {
    try {
      // const query = id + '?include=field_hero_image,field_project_timeline,field_project_intro_image'
      const query = id + '?include=field_hero_image,field_project_intro_image,field_project_timeline,field_project_timeline.field_project_tl_layout,field_project_timeline.field_project_tl_layout.field_timeline_image'
      const resp = await this.$portfolioRepository.show(query)
      const data = resp.data
      const included = resp.included
      let heroImage = {}
      let introImage = {}
      const timeline = []
      included.forEach((inc) => {
        if (inc.id === data.relationships.field_hero_image.data.id) {
          heroImage = {
            alt: data.relationships.field_hero_image.data.meta.alt,
            url: inc.attributes.uri.url
          }
        }
        if (inc.id === data.relationships.field_project_intro_image.data.id) {
          introImage = {
            alt: data.relationships.field_project_intro_image.data.meta.alt,
            url: inc.attributes.uri.url
          }
        }
        if (inc.type === 'paragraph--project_tl_left' || inc.type === 'paragraph--project_tl_right') {
          // get all the relationship IDs.
          timeline.push({
            title: inc.attributes.field_timeline_item_title,
            description: inc.attributes.field_timeline_item_description.processed,
            image_id: inc.relationships.field_timeline_image.data.id,
            image: {
              alt: inc.relationships.field_timeline_image.data.meta.alt
            }
          })
        }
      })
      // Second loop for timeline image
      if (timeline.length > 0) {
        timeline.forEach((tl, index) => {
          included.forEach((inc) => {
            if (inc.id === tl.image_id) {
              timeline[index].image.url = inc.attributes.uri.url
            }
          })
        })
      }
      commit('setCurrentProject', {
        projectData: resp.data,
        images: {
          intro: introImage,
          hero: heroImage
        },
        timeLine: timeline
      })
    } catch (err) {
      // eslint-disable-next-line
      console.log(err)
    }
  },
  removeCurrentProject ({ state, commit }) {
    commit('removeCurrentProject')
  }
}
