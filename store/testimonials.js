export const state = () => ({
  testimonials: []
})

export const mutations = {
  set (state, testimonials) {
    state.testimonials = testimonials
  },
  add (state, value) {
    state.testimonials.push(value)
  },
  remove (state, { testimonial }) {
    state.testimonial.splice(state.list.indexOf(testimonial), 1)
  }
}

export const actions = {
  async getTestimonials ({ state, commit }) {
    if (state.testimonials.length) {
      return
    }
    try {
      const fields = 'fields[node--reviews]=title,body,field_review_source,field_reviewer_company,field_star_rating'
      const query = `?${fields}`
      const resp = await this.$testimonialRepository.show(query)
      const data = resp.data
      const testimonials = []
      data.forEach((item) => {
        testimonials.push({
          id: item.id,
          title: item.attributes.title,
          body: item.attributes.body.processed,
          source: item.attributes.field_review_source,
          company: item.attributes.field_reviewer_company,
          rating: item.attributes.field_star_rating
        })
      })
      commit('set', testimonials)
    } catch (err) {
      // eslint-disable-next-line
      console.log(err)
    }
  }
}
