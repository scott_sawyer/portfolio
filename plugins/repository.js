import createRepository from '~/api/repository'
export default (ctx, inject) => {
  const repositoryWithAxios = createRepository(ctx.$axios)
  // Blogs.
  inject('blogRepository', repositoryWithAxios('/node/blog'))
  // Pages.
  inject('pageRepository', repositoryWithAxios('/node/page'))
  // Webforms.
  inject('formRepository', repositoryWithAxios('/webform/'))
  // Portfolio.
  inject('portfolioRepository', repositoryWithAxios('/node/project'))
  // Testimonials.
  inject('testimonialRepository', repositoryWithAxios('node/reviews'))
}
