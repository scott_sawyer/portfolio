export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Scott Sawyer Portfolio',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { theme_color: '#f6f7f8' },
      { naviveUI: 'true' },
      { author: 'Scott Sawyer' },
    ],
    link: [
      { rel: 'icon', type: 'image/svg+xml', href: 'data:image/svg+xml,%3Csvg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" id="favicon" version="1.1" viewBox="0 0 33.866665 33.866667" height="128" width="128"%3E%3Cstyle%3E %23border %7B fill:%23ba0096; %7D %23sun %7B fill:%230f59ff; %7D %23mountains %7B fill:%2300871a; %7D %23background %7B fill: %23ffffff; %7D @media (prefers-color-scheme: dark) %7B %23border, %23sun, %23mountains %7B fill: %23ffffff; %7D %23background %7B fill: transparent; %7D %7D %3C/style%3E%3Cg id="layer1"%3E%3Cpath id="background" d="M 3.9687486,3.4395794 H 29.104166 c 0.732896,0 1.322917,0.59002 1.322917,1.32292 V 29.897909 c 0,0.7329 -0.590021,1.32292 -1.322917,1.32292 H 3.9687486 c -0.732895,0 -1.322916,-0.59002 -1.322916,-1.32292 V 4.7624994 c 0,-0.7329 0.590021,-1.32292 1.322916,-1.32292 z" style="opacity:1;fill-opacity:1;stroke:none;" /%3E%3Cpath id="border" d="m 2.6458326,2.1166694 c -0.732898,0 -1.322916,0.59001 -1.322916,1.32291 V 31.220829 c 0,0.7329 0.590018,1.32292 1.322916,1.32292 H 30.427083 c 0.732898,0 1.322917,-0.59002 1.322917,-1.32292 V 3.4395794 c 0,-0.7329 -0.590019,-1.32291 -1.322917,-1.32291 z m 1.322917,1.32291 H 29.104166 c 0.732896,0 1.322917,0.59002 1.322917,1.32292 V 29.897919 c 0,0.7329 -0.590021,1.32291 -1.322917,1.32291 H 3.9687496 c -0.732895,0 -1.322917,-0.59001 -1.322917,-1.32291 V 4.7624994 c 0,-0.7329 0.590022,-1.32292 1.322917,-1.32292 z" style="opacity:1;fill-opacity:1;stroke:none;" /%3E%3Cpath id="mountains" d="m 13.125584,15.345839 c -0.446426,0 -0.892764,0.27125 -1.234837,0.8142 l -7.4097394,11.76088 c -0.344818,0.54732 -0.515016,1.26281 -0.51225,1.977 H 27.781224 c -0.0042,-0.41657 -0.105172,-0.83125 -0.306205,-1.15032 l -4.445699,-7.0563 c -0.41049,-0.65154 -1.071173,-0.65154 -1.481662,0 l -1.85154,2.93766 -5.335696,-8.46892 c -0.342075,-0.54295 -0.788414,-0.8142 -1.234838,-0.8142 z" style="opacity:1;fill-opacity:1;stroke:none;" /%3E%3Ccircle r="5.2916665" cy="10.054163" cx="23.812496" id="sun" style="opacity:1;fill-opacity:1;stroke:none;" /%3E%3C/g%3E%3C/svg%3E' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Nunito&display=swap' }
    ]
  },
  pwa: {
    manifest: {
      name: 'Scott Sawyer Portfolio',
      lang: 'en',
      theme_color: '#f6f7f8',
      background_color: '#ffffff',
      short_name: 'Portfolio',
      start_url: '/'
    },
    workbox: {
      cacheNmes: {
        prefix: 'Scott Sawyer'
      },
      runtimeCaching: [
        {
          urlPattern: '/.nuxt/',
          handler: 'cacheFirst',
          method: 'GET'
        },
        {
          urlPattern: 'https://www.scottsawyerconsulting.com/jsonapi/*',
          handler: 'staleWhileRevalidate',
          method: 'GET',
          strategyOptions: {
            cacheName: 'jsonapi',
            cacheableResponse: { statuses: [0, 200] },
            cacheExpiration: {
              maxEntries: 10,
              maxAgeSeconds: 0
            }
          }
        },
        {
            urlPattern: 'https://fonts.googleapis.com/.*',
            handler: 'cacheFirst',
            method: 'GET',
            strategyOptions: {cacheableResponse: {statuses: [0, 200]}}
        },
        {
            urlPattern: 'https://fonts.gstatic.com/.*',
            handler: 'cacheFirst',
            method: 'GET',
            strategyOptions: {cacheableResponse: {statuses: [0, 200]}}
        },
      ]
    },
    icon: {
      source: '~/static/icon.png',
      fileName: 'icon.png'
    }
  },
  // Loading animation.
  loading: {
    color: '#0f59ff',
    height: '3px'
  },
  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '~/assets/scss/styles.scss'
  ],
  styleResources: {
    scss: ['./assets/scss/_base.scss']
  },
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/youtube', ssr: false },
    { src: '~/plugins/repository' }
  ],
  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,
  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    '@nuxtjs/style-resources'
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: 'https://www.scottsawyerconsulting.com/jsonapi'
  },

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: ['vue-youtube-embed']
  }
}
