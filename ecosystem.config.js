module.exports = {
  apps: [
    {
      name: 'Portfolio',
      exec_interpreter: "/home/scott/.nvm/versions/node/v12.18.3/bin/node",
      //script: './node_modules/nuxt/bin/nuxt.js',
      script: './node_modules/@nuxt/cli/bin/nuxt-cli.js',
      port: 3500,
      args: ['start'],
      env: {
        PORT: 3500,
        NODE_ENV: 'production'
      }
    }
  ]
}
