"use strict";

const Config = {
  appTitle: "Scott Sawyer",
  appTitleShort: "Scott Sawyer",
  apiDomain: "https://www.scottsawyeerconsulting.com",
  api: {
    blogs: "/blogs/rest?format=json",
    contactForm: '/webform/{contact}?format=json",
    portfolioPages: '/jsonapi/node/portfolio"
  }
}

module.exports = Config;
