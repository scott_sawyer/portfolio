<div>
  <article class="story">
    <div class="section-content-container">
      <h2 class="section-title">
        My Story
      </h2>
      <div class="section-content">
        <p>I started a boutique Web Design / Development agency in 2005, and specialized in the custom stuff, projects that businesses couldn't seem to find a developer who could deliver their vision.  I've worked with many frameworks, from CakePHP to Magento, opensource and proprietary LMSes, even OSCommerce ( once upon a time ). After developing my own PHP framework, I discovered Drupal and never looked back. I even went so far as to create a <a href="https://bitbucket.org/wpbuilderplugin/wp-builder/src/2.0.3/" target="_blank" rel="noopener" title="WP Builder">WordPress plugin</a> to make it just a bit more like Drupal.</p>
        <p>As an active member of the Drupal community, I co-maintain <a href="https://www.drupal.org/project/commerce_printful" target="_blank" rel="noopener">Commerce Printful</a>, contribute patches, <a href="https://www.drupal.org/u/scottsawyer/issue-credits" target="_blank" rel="noopener">help in the issue queues</a>, and present at DrupalCamp ATL (<a href="https://www.drupalcampatlanta.com/2020/sessions/entity-reference-layout-may-be-zen-editorial-layout-control" target="_blank" rel="noopener">2020</a>, <a href="https://www.drupalcampatlanta.com/2016-drupalcamp-atlanta/sessions/foundation-paragraphs-display-suite-super-awesomeness" target="_blank" rel="noopener">2016</a>).  I enjoy helping others learn Drupal as well as expanding my own capabilities.</p>
        <p>Building an agency taught me business skills beyond software development.  I learned to build and manage a team ( at one time employing 2 devs, a designer, and a copywriter ), run profitable projects, and hone my communication skills, particularly with non-developer stakeholders.  These skills could add a lot of value to your organization.</p>
        <p>After 15 years operating an agency, I was ready for a new challenge.  I accepted a position as a Senior Drupal Developer at a marketing firm and was quickly promoted to Architect.  Over the next two years, I worked through the backlog of Drupal projects, mostly highly customized Drupal Commerce implementations, ERP integrations, and APIs.  My primary focus has been creating value by modernizing editorial experiences and improving front-end performance. Working with this team has been a great experience.</p>
        <p>Now it’s time for a new adventure.  I am seeking a Senior Drupal Developer position at a great company or organization.  Ideally, it will be a place where my skills and experience can make a positive impact with growth opportunities.  I’d love to work with other professionals that share my passion for creating and collaborating.  If that is you, please <a href="/contact">contact me</a>.</p>
      </div>
      <blockquote class="quote">
        <p>Passion for business, heart of a teacher</p>
      </blockquote>
    </div>
  </article>
</div>
