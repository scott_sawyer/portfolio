<article>
  <div class="resume">
  <h2 class="section-title">
    My Resume
  </h2>
  <section class="resume-section experience">
    <h3 class="resume-section-title">
      Experience
    </h3>
    <ul class="experience-list">
      <li>
        <h4 class="experience-title">
          Top Floor &mdash; <em>Senior Drupal Developer</em>
        </h4>
        <p class="experience-meta">
          January 2021 - PRESENT
        </p>
        <p>
          <a href="https://www.topfloortech.com/team-member/scott-sawyer/" target="_blank" rel="noopener" class="experience-link">https://www.topfloortech.com</a>
        </p>
        <p class="experience-description">
          <strong>Developer role:</strong> Designed API integrations with ERPs and highly customized  Drupal Commerce implementations. Custom integrations with Pardot, Salesforce, and PIMs. Drupal 7 to Drupal 9 migrations.  
          <strong>Management role:</strong> Department procedures, project estimates, requirements gathering, scope management.
        </p>
        <ul class="project-skills">
          <li>Circle CI pipelines</li>
          <li>Design / Build Drupal Commerce applications with > 100k SKUs</li>
          <li>ERP / Marketing automation integrations</li>
          <li>Maintain portfolio of custom modules</li>
          <li>Drupal 7 to Drupal 9 migrations</li>
        </ul>
      </li>
      <li>
        <h4 class="experience-title">
          Rate Calculator &mdash; <em>Developer</em>
        </h4>
        <p class="experience-meta">
          April 2020 - PRESENT
        </p>
        <p>
          <a href="https://rate-calculator.com" target="_blank" rel="noopener" class="experience-link">https://rate-calculator.com/</a>
        </p>
        <p class="experience-description">
          Created subscription WordPress plugin using Drupal 8 as the back-end API and subscription management system.  Leverages OAuth2 and custom REST plugin for securing API communication with 3rd party services.
        </p>
        <ul class="project-skills">
          <li>Custom Drupal modules and theme.</li>
          <li>Drupal Commerce, Drupal Recurring Framework, custom Commerce Checkout process.</li>
          <li>JSONAPI, Simple OAuth2, custom REST plugin.</li>
        </ul>
      </li>
      <li>
        <h4 class="experience-title">
          The Travel Store &mdash; <em>Developer</em>
        </h4>
        <p class="experience-meta">
          August 2018 - PRESENT
        </p>
        <p><a href="https://thetravelstorefieldtrips.com" target="_blank" rel="noopener" class="experience-link">https://thetravelstorefieldtrips.com/</a></p>
        <p class="experience-description">
          Created Drupal 8 based line of business platform to replace multiple inhouse applications. Custom modules for task tracking, calendaring, auditing, resource management.
        </p>
        <ul class="project-skills">
          <li>Custom Drupal modules and theme.</li>
          <li>Custom calendars, reporting and auditing tools.</li>
          <li>Queue Worker, Layout plugins, Views plugins, custom Workflows.</li>
        </ul>
      </li>
      <li>
        <h4 class="experience-title">
          Mulligan Disc Golf &mdash; <em>Developer</em>
        </h4>
        <p class="experience-meta">
          April 2019 - PRESENT
        </p>
        <p><a href="https://mulligandisc.golf" target="_blank" rel="noopener" class="experience-link">https://mulligandisc.gold</a></p>
        <p class="experience-description">
          Created Drupal Commerce drop-ship site that integrates with Printful. I developed the Drush commands for the Commerce Printful module and have contributed several patches since launch.
        </p>
        <ul class="project-skills">
          <li>Custom Drupal modules and theme.</li>
          <li>Contributed Drush 9 / 10 commands to Commerce Printful.</li>
          <li>Drupal Commerce, Commerce Stripe</li>
        </ul>
      </li>
      <li>
        <h4 class="experience-title">
          TrueGolfFit &mdash; <em>Contract Developer</em>
        </h4>
        <p class="experience-meta">
          April 2017 &ndash; PRESENT
        </p>
        <p><a href="https://truegolffit.com" target="_blank" rel="noopener" class="experience-link">https://truegolffit.com</a></p>
        <p class="experience-description">
          Created a golf club recommendation application using Drupal 8.  Custom built data importer processes thousands of data points, performs statistical analysis on the data used in the proprietary recommendation engine.
        </p>
        <ul class="project-skills">
          <li>Custom modules and theme.</li>
          <li>Custom Drush commands, custom Drupal entities.</li>
          <li>Marketing automation integration.</li>
          <li>Dynamic SVGs, animations, video.</li>
        </ul>
      </li>
    </ul>
  </section>
  <section class="resume-section education">
    <h3 class="resume-section-title">
      Education
    </h3>
    <h4 class="education-title">
      Southern Polytechnic State University &mdash; <em>Marietta, Ga</em>
    </h4>
    <p>B.S. Information Technology</p>
    <p class="experience-meta">
      May 2005
    </p>
  </section>
  <section class="resume-section skills">
    <h3 class="resume-section-title">
      Skills
    </h3>
    <ul class="skills-list">
      <li>PHP, JavaScript, CSS, SASS</li>
      <li>Drupal, WordPress</li>
      <li>Front-end, Back-end, design and development</li>
      <li>Custom modules, plugins and themes</li>
      <li>Commerce, APIs / integrations, migrations</li>
      <li>Server technologies: Pantheon, DigitalOcean, NGINX, Apache, MySQL, Ubuntu, Git, CircleCi</li>
      <li>Credited on 33 fixed issues on Drupal.org.</li>
    </ul>
  </section>
  <section class="resume-section projects">
    <h3 class="resume-section-title">
      Open Source Projects
    </h3>
    <ul class="projects-list">
      <li>
        <h4 class="project-title">
          Commerce Printful &mdash; <em>Drupal Module</em>
        </h4>
        <a href="https://www.drupal.org/project/commerce_printful" target="_blank" rel="noopener">
         https://www.drupal.org/project/commerce_printful
        </a>
      </li>
      <li>
        <h4 class="project-title">
          Bitly &mdash; <em>Drupal Module</em>
        </h4>
        <a href="https://www.drupal.org/sandbox/scottsawyer/2845808" target="_blank" rel="noopener">
          https://www.drupal.org/sandbox/scottsawyer/2845808
        </a>
      </li>
      <li>
        <h4 class="project-title">
          CMB2 Field Address &mdash; <em>WordPress Plugin</em>
        </h4>
        <a href="https://github.com/scottsawyer/cmb2-field-address" target="_blank" rel="noopener">
          https://github.com/scottsawyer/cmb2-field-address
        </a>
      </li>
      <li>
        <h4 class="project-title">
          CMB2 Field Widget Selector &mdash; <em>WordPress Plugin</em>
        </h4>
        <a href="https://github.com/scottsawyer/cmb2-field-widget-selector" target="_blank" rel="noopener">
          https://github.com/scottsawyer/cmb2-field-widget-selector
        </a>
      </li>
    </ul>
  </section>
  </div>
</article>
